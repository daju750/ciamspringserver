package pro.ciam.server.jwt;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.core.exc.StreamReadException;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.ObjectMapper;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import pro.ciam.server.entity.UsuarioEntity;

public class JwtAthenticationFilter extends UsernamePasswordAuthenticationFilter {

	private JwtUtils jwtUtils;
	
	public JwtAthenticationFilter(JwtUtils jwtUtils) {
		this.jwtUtils=jwtUtils;
	}
	
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, 
			HttpServletResponse response)throws AuthenticationException {
		UsuarioEntity usuario = null;
		String username="",password="";
		
				try {
					usuario = new ObjectMapper().readValue(request.getInputStream(),UsuarioEntity.class);
					username = usuario.getUsername();
					password = usuario.getPassword();
				} catch (StreamReadException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (DatabindException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				UsernamePasswordAuthenticationToken authtoken = new UsernamePasswordAuthenticationToken(username,password);
				
		return getAuthenticationManager().authenticate(authtoken);
		//return super.attemptAuthentication(request, response);
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication authResult) throws IOException, ServletException {
		
		User user = (User) authResult.getPrincipal();
		String token = this.jwtUtils.generarAccesToken(user.getUsername());
		
		response.addHeader("Autozation", token);
		
		Map<String,Object> httpResponse = new HashMap<>();
		
		httpResponse.put("token",token);
		httpResponse.put("message","Authenticacion correcta");
		httpResponse.put("usuario",user.getUsername());
		
		response.getWriter().write(new ObjectMapper().writeValueAsString(httpResponse));
		response.setStatus(HttpStatus.OK.value());
		response.setContentType(MediaType.APPLICATION_JSON_VALUE);
		response.getWriter().flush();
		
		super.successfulAuthentication(request, response, chain, authResult);
	}
	
}
