package pro.ciam.server.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.PrePersist;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @AllArgsConstructor @NoArgsConstructor
@Entity @Builder
@Table(name="persona")
public class PersonaEntity implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id",unique=true)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotBlank
	@Column(name="nombres_completos",length = 80)
	private String nombres_completos;
	
	@NotBlank
	@Column(name="apellidos_completos",length = 80)
	private String apellidos_completos;
	
	@NotBlank
	@Column(name="identificacion",length = 80)
	private String identificacion;
	
	@NotBlank
	@Column(name="tipo_identificacion",length = 80)
	private String tipo_identificacion;
	
	@Column(name="fecha_nacimiento")
	private Date fecha_nacimiento;

	@NotBlank
	@Column(name="genero")
	private String genero;

	@NotBlank
	@Column(name="direccion",length = 80)
	private String direccion;
	
	@NotBlank
	@Column(name="departamento",length = 80)
	private String departamento;
	
	@NotBlank
	@Column(name="municipio",length = 80)
	private String municipio;
	
	@Column(name="telefono",length = 16)
	private Integer telefono;
	
	@Email
	@NotBlank
	@Column(name="email",length = 80)
	private String email;
	
	@NotBlank
	@Column(name="tipo_paciente")
	private String tipo_paciente;
	
	@Column(name="fecha_creacion")
	private Date fecha_creacion;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@ManyToMany(fetch= FetchType.EAGER, targetEntity = RolEntity.class, cascade = CascadeType.MERGE)
	@JoinTable(name = "persona_roles", joinColumns = @JoinColumn(name = "persona_id"), inverseJoinColumns = @JoinColumn(name = "rol_id"))
	private List<RolEntity> roles;
	
	public void save() {
	    createdAt();
	}
	
	@PrePersist
	void createdAt() {
	    this.fecha_creacion = new Date();
	}

}
