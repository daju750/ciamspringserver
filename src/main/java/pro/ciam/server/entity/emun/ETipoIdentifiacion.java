package pro.ciam.server.entity.emun;

public enum ETipoIdentifiacion {

	CEDULA("CEDULA"),
	DPI("DPI");
	
	private final String name; 
	
    private ETipoIdentifiacion(String s) {
        name = s;
    }

    public boolean equalsName(String otherName) {
        return name.equals(otherName);
    }

    public String toString() {
       return this.name;
    }
	
}
