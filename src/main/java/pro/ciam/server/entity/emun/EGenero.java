package pro.ciam.server.entity.emun;

public enum EGenero {
	MASCULINO("MASCULINO"),
	FEMENINO("FEMENINO");

	private final String name; 
	
    private EGenero(String s) {
        name = s;
    }

    public boolean equalsName(String otherName) {
        return name.equals(otherName);
    }

    public String toString() {
       return this.name;
    }
}
