package pro.ciam.server.entity.emun;

public enum ETipoPaciente {
	VIP("VIP"),
	ESTANDAR("ESTANDAR");
	
	private final String name; 
	
    private ETipoPaciente(String s) {
        name = s;
    }

    public boolean equalsName(String otherName) {
        return name.equals(otherName);
    }

    public String toString() {
       return this.name;
    }
}
