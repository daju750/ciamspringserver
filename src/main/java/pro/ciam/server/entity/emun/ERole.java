package pro.ciam.server.entity.emun;

public enum ERole {
	ADMIN_ROL,
	USUARIO_ROL,
	PACIENTE_ROL,
	PACIENTE_POTENCIAL_ROL
}
