package pro.ciam.server.entity;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pro.ciam.server.entity.emun.ERole;

@Data @AllArgsConstructor @NoArgsConstructor
@Entity @Builder
@Table(name="rol")
public class RolEntity implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="nombre", length = 25)
	@NotBlank
	@Enumerated(EnumType.STRING)
	private ERole nombre;

	
}
