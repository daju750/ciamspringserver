package pro.ciam.server.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import pro.ciam.server.entity.PersonaEntity;

public interface PersonaRepositorio extends JpaRepository<PersonaEntity,Long>{

	void save(Optional<PersonaEntity> persona);

}
