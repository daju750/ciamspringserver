package pro.ciam.server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import pro.ciam.server.entity.RolEntity;

public interface RolRepositorio extends JpaRepository<RolEntity,Long>{

	@Query(value="select u.id from rol u where u.nombre=?1",nativeQuery = true)
	Long id(String nombre);
	
}
