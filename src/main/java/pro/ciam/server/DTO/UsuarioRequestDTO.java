package pro.ciam.server.DTO;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @AllArgsConstructor @NoArgsConstructor
public class UsuarioRequestDTO {

	@NotBlank
	@JsonProperty(value = "username")
	private String username;
	@NotBlank
	@JsonProperty(value = "password")
	private String password;
	@NotBlank
	@JsonProperty(value = "rol")
	private String rol;
	
}
