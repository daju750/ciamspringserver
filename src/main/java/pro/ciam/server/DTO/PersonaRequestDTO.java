package pro.ciam.server.DTO;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @AllArgsConstructor @NoArgsConstructor
public class PersonaRequestDTO {

	@NotBlank
	@JsonProperty(value = "id")
	private Long id;
	@Email
	@NotBlank
	@JsonProperty(value = "nombres_completos")
	private String nombres_completos;
	@NotBlank
	@JsonProperty(value = "apellidos_completos")
	private String apellidos_completos;
	@NotBlank
	@JsonProperty(value = "identificacion")
	private String identificacion;
	@NotBlank
	@JsonProperty(value = "tipo_identificacion")
	private String tipo_identificacion;
	@NotBlank
	@JsonProperty(value = "fecha_nacimiento")
	private String fecha_nacimiento;
	@NotBlank
	@JsonProperty(value = "genero")
	private String genero;
	@NotBlank
	@JsonProperty(value = "direccion")
	private String direccion;
	@NotBlank
	@JsonProperty(value = "departamento")
	private String departamento;
	@NotBlank
	@JsonProperty(value = "municipio")
	private String municipio;
	@JsonProperty(value = "telefono")
	private Integer telefono;
	@Email
	@NotBlank
	@JsonProperty(value = "email")
	private String email;
	@NotBlank
	@JsonProperty(value = "tipo_paciente")
	private String tipo_paciente;
	@NotBlank
	@JsonProperty(value = "rol")
	private String rol;	
}
