package pro.ciam.server.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @AllArgsConstructor @NoArgsConstructor
public class ValidarTokenResponseDTO {

	Boolean ok;
	String message;
	
}
