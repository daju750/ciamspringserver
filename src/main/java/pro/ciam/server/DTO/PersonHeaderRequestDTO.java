package pro.ciam.server.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @AllArgsConstructor @NoArgsConstructor
public class PersonHeaderRequestDTO {
	
	Long id;
	String identificacion;
	String tipo_identificacion;
	String nombres_completos;
	String apellidos_completos;
	String email;
	
}