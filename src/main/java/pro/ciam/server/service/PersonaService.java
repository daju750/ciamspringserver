package pro.ciam.server.service;

import org.springframework.http.ResponseEntity;

import pro.ciam.server.DTO.PersonaRequestDTO;

public interface PersonaService {

	public ResponseEntity<?> Guardar(PersonaRequestDTO personadto);
	public ResponseEntity<?> Actulizar(PersonaRequestDTO personadto);
	public ResponseEntity<?> ListaPersonas();
	public ResponseEntity<?> PersonaporId(Long id);
	public ResponseEntity<?> BorrarPersonaporId(Long id);
	
}
