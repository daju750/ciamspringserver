package pro.ciam.server.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import pro.ciam.server.DTO.UsuarioRequestDTO;
import pro.ciam.server.entity.RolEntity;
import pro.ciam.server.entity.UsuarioEntity;
import pro.ciam.server.entity.emun.ERole;
import pro.ciam.server.repository.UsuarioRepositorio;
import pro.ciam.server.service.UsuarioService;

@Service
public class UsuarioServiceImpl implements UsuarioService{

	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private RolServiceImpl rolimpl;
	
	@Autowired
	private UsuarioRepositorio usuariorepo;
	
	public ResponseEntity<?> guardar(UsuarioRequestDTO usuariodto) {
		
		Map<String,Object> httpResponse = new HashMap<>();
		
		try{
			
			RolEntity rol = new RolEntity(this.rolimpl.IdporNombre(usuariodto.getRol()),ERole.valueOf(usuariodto.getRol())); 
			List<RolEntity> roles = new ArrayList<>(Arrays.asList(rol));
			
			UsuarioEntity usuario = new UsuarioEntity();
			usuario.setPassword(this.passwordEncoder.encode(usuariodto.getPassword()));
			usuario.setUsername(usuariodto.getUsername());
			usuario.setRoles(roles);

			httpResponse.put("message","Usuario Guardado Correctamente");
			httpResponse.put("Usuario",this.usuariorepo.save(usuario));
			
			return ResponseEntity.status(HttpStatus.OK).body(httpResponse);
			
		}catch(Exception ex) {
			
			httpResponse.put("message","Usuario no Ingresado");
			httpResponse.put("Detalle",ex);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(httpResponse);
		
		}
		
		
	}

}
