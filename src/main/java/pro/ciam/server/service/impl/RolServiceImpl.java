package pro.ciam.server.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pro.ciam.server.repository.RolRepositorio;
import pro.ciam.server.service.RolService;

@Service
public class RolServiceImpl implements RolService{

	@Autowired
	RolRepositorio rolrepo;

	public Long IdporNombre(String nombre) {
		try{return this.rolrepo.id(nombre);
		}catch(Exception ex){return (long)0;}
	}

}
