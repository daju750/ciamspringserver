package pro.ciam.server.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import pro.ciam.server.DTO.PersonHeaderRequestDTO;
import pro.ciam.server.DTO.PersonaRequestDTO;
import pro.ciam.server.entity.PersonaEntity;
import pro.ciam.server.entity.RolEntity;
import pro.ciam.server.entity.emun.EGenero;
import pro.ciam.server.entity.emun.ERole;
import pro.ciam.server.entity.emun.ETipoIdentifiacion;
import pro.ciam.server.entity.emun.ETipoPaciente;
import pro.ciam.server.repository.PersonaRepositorio;
import pro.ciam.server.service.PersonaService;

@Service
public class PersonaServiceImpl implements PersonaService{

	@Autowired
	private RolServiceImpl rolimpl;
	
	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private PersonaRepositorio personarepo;
	
	public ResponseEntity<?> Guardar(PersonaRequestDTO personadto) {
		
		try {
			
			RolEntity rol = new RolEntity(this.rolimpl.IdporNombre(personadto.getRol()),ERole.valueOf(personadto.getRol())); 
			List<RolEntity> roles = new ArrayList<>(Arrays.asList(rol));
			
			PersonaEntity persona = new PersonaEntity();
			
			persona.setNombres_completos(personadto.getNombres_completos());
			persona.setApellidos_completos(personadto.getApellidos_completos());
			persona.setIdentificacion(personadto.getIdentificacion());
			persona.setTipo_identificacion(ETipoIdentifiacion.valueOf(personadto.getTipo_identificacion()).name());
			persona.setFecha_nacimiento(new SimpleDateFormat("dd/MM/yyyy").parse(personadto.getFecha_nacimiento()));
			persona.setGenero(EGenero.valueOf(personadto.getGenero()).name());
			persona.setDireccion(personadto.getDireccion());
			persona.setDepartamento(personadto.getDepartamento());
			persona.setMunicipio(personadto.getMunicipio());
			persona.setTelefono(Integer.valueOf(personadto.getTelefono()));
			persona.setEmail(personadto.getEmail());
			persona.setTipo_paciente(ETipoPaciente.valueOf(personadto.getTipo_paciente()).name());
			persona.setRoles(roles);
			persona.save();
			
			this.personarepo.save(persona);
			
			return ResponseEntity.ok(persona);
			
		}catch(Exception ex) {
			
			Map<String,Object> httpResponse = new HashMap<>();
			httpResponse.put("message","Persona no Ingresada");
			httpResponse.put("Detalle",ex);
			
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(httpResponse);
			
		}
	}

	@SuppressWarnings("deprecation")
	public ResponseEntity<?> ListaPersonas() {

		try{
			
		List<PersonHeaderRequestDTO> personalistdto = this.jdbcTemplate.query("select p.id,p.identificacion,p.tipo_identificacion,p.nombres_completos,p.apellidos_completos,p.email from persona p where p.id in(select persona_id from persona_roles where rol_id in(select id from rol where nombre='PACIENTE_ROL' or nombre='PACIENTE_POTENCIAL_ROL'))",
					new Object[]{},
					(rs, rowNum) -> new PersonHeaderRequestDTO(
					rs.getLong("id"),
					rs.getString("identificacion"),
					rs.getString("tipo_identificacion"),
					rs.getString("nombres_completos"),
					rs.getString("apellidos_completos"),
					rs.getString("email")
					));


			return ResponseEntity.ok(personalistdto);
		
		}catch(Exception ex){
			
			Map<String,Object> httpResponse = new HashMap<>();
			httpResponse.put("message","Error de consulta");

			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(httpResponse);
		}
		
		
		
	}

	public ResponseEntity<?> PersonaporId(Long id) {
		try{
			
			PersonaRequestDTO personarest = new PersonaRequestDTO(); 
			PersonaEntity persona = this.personarepo.findById(id).get();
			personarest.setId(persona.getId());
			personarest.setNombres_completos(persona.getNombres_completos());
			personarest.setApellidos_completos(persona.getApellidos_completos());
			personarest.setIdentificacion(persona.getIdentificacion());
			personarest.setTipo_identificacion(persona.getTipo_identificacion());
			String fecha[] = persona.getFecha_nacimiento().toString().split(" ")[0].split("-");
			personarest.setFecha_nacimiento(fecha[2]+"/"+fecha[1]+"/"+fecha[0]);
			personarest.setGenero(persona.getGenero());
			personarest.setDireccion(persona.getDireccion());
			personarest.setDepartamento(persona.getDepartamento());
			personarest.setMunicipio(persona.getMunicipio());
			personarest.setTelefono(persona.getTelefono());
			personarest.setTipo_paciente(persona.getTipo_paciente());
			personarest.setEmail(persona.getEmail());
			personarest.setRol(persona.getRoles().get(0).getNombre().toString());
			
			return ResponseEntity.ok(personarest);
			
		}catch(Exception ex){
			
			Map<String,Object> httpResponse = new HashMap<>();
			httpResponse.put("message","Persona no encontrada");
			return ResponseEntity.status(HttpStatus.BAD_GATEWAY).body(httpResponse);
		}
	}

	
	public ResponseEntity<?> BorrarPersonaporId(Long id) {
		Map<String,Object> httpResponse = new HashMap<>();
		try{
			this.personarepo.deleteById(id);
			httpResponse.put("id",id);
			httpResponse.put("message","Persona Borrada Exitosamente");
			return ResponseEntity.status(HttpStatus.OK).body(httpResponse);
			
		}catch(Exception ex) {
			
			httpResponse.put("id",id);
			httpResponse.put("message","Persona no encontrada");
			return ResponseEntity.status(HttpStatus.BAD_GATEWAY).body(httpResponse);

		}
	}


	public ResponseEntity<?> Actulizar(PersonaRequestDTO personadto) {
		
		try {
			
			RolEntity rol = new RolEntity(this.rolimpl.IdporNombre(personadto.getRol()),ERole.valueOf(personadto.getRol())); 
			List<RolEntity> roles = new ArrayList<>(Arrays.asList(rol));
			
			PersonaEntity persona = this.personarepo.findById(personadto.getId()).get();
			
			persona.setNombres_completos(personadto.getNombres_completos());
			persona.setApellidos_completos(personadto.getApellidos_completos());
			
			persona.setIdentificacion(personadto.getIdentificacion());
			persona.setTipo_identificacion(ETipoIdentifiacion.valueOf(personadto.getTipo_identificacion()).name());
			persona.setFecha_nacimiento(new SimpleDateFormat("dd/MM/yyyy").parse(personadto.getFecha_nacimiento()));
			persona.setGenero(EGenero.valueOf(personadto.getGenero()).name());
			persona.setDireccion(personadto.getDireccion());
			persona.setDepartamento(personadto.getDepartamento());
			persona.setMunicipio(personadto.getMunicipio());
			persona.setTelefono(Integer.valueOf(personadto.getTelefono()));
			persona.setEmail(personadto.getEmail());
			persona.setTipo_paciente(ETipoPaciente.valueOf(personadto.getTipo_paciente()).name());
			persona.setRoles(roles);
			persona.save();
			
			this.personarepo.save(persona);
			
			return ResponseEntity.ok(persona);
			
		} catch(Exception ex) {
			
			Map<String,Object> httpResponse = new HashMap<>();
			httpResponse.put("message","Persona no actulizada");
			httpResponse.put("Detalle",ex);
			
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(httpResponse);
		}
	}
	

}
