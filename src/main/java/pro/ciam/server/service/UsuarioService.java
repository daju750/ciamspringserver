package pro.ciam.server.service;

import org.springframework.http.ResponseEntity;

import pro.ciam.server.DTO.UsuarioRequestDTO;

public interface UsuarioService {

	public ResponseEntity<?> guardar(UsuarioRequestDTO usuariodto);
	
}
