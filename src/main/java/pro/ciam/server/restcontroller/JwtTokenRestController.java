package pro.ciam.server.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pro.ciam.server.DTO.ValidarTokenResponseDTO;
import pro.ciam.server.jwt.JwtUtils;

@RestController
@RequestMapping("/jwt")
public class JwtTokenRestController {

	@Autowired
	private JwtUtils jwtUtils;
	
	@PostMapping
	@ResponseBody
	public ValidarTokenResponseDTO GetListar(@RequestBody String token){
		ValidarTokenResponseDTO validartoken = new ValidarTokenResponseDTO();
		try {
			//JSONObject json = new JSONObject(token);
			validartoken.setOk(this.jwtUtils.isTokenValid(token));
			validartoken.setMessage("Token Valido");
			return validartoken;
		}catch(Exception ex){
			validartoken.setOk(false);
			validartoken.setMessage("Token Invalido");
			return validartoken;
		}
		
	}
	
}
