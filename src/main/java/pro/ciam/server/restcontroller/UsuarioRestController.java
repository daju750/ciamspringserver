package pro.ciam.server.restcontroller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pro.ciam.server.DTO.UsuarioRequestDTO;
import pro.ciam.server.service.impl.UsuarioServiceImpl;


@RestController
@RequestMapping("/usuario")
public class UsuarioRestController {
	
	@Autowired
	private UsuarioServiceImpl usuarioimpl;
	
	@PostMapping
	@ResponseBody
	public ResponseEntity<?> InsertarUsuario(@Valid @RequestBody UsuarioRequestDTO usuariodto){
		return this.usuarioimpl.guardar(usuariodto);
	}
	
}
