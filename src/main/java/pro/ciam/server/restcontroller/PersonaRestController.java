package pro.ciam.server.restcontroller;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pro.ciam.server.DTO.PersonaRequestDTO;
import pro.ciam.server.service.impl.PersonaServiceImpl;

@RestController
@RequestMapping("/persona")
public class PersonaRestController {

	@Autowired
	private PersonaServiceImpl personaimpl;

	@PostMapping("/nueva")
	@ResponseBody
	public ResponseEntity<?> InsertarUsuario(@Valid @RequestBody PersonaRequestDTO personadto){
		return this.personaimpl.Guardar(personadto);
	}
	
	@PostMapping("/actualizar/{id}")
	@ResponseBody
	public ResponseEntity<?> ActulizarUsuario(@Valid @RequestBody PersonaRequestDTO personadto){
		System.out.print(personadto);
		return this.personaimpl.Actulizar(personadto);
	}
	
	@GetMapping("/lista")
	@ResponseBody
	public ResponseEntity<?> ListaUsuario(){
		return this.personaimpl.ListaPersonas();
	}
	
	@GetMapping("/buscar/{id}")
	@ResponseBody
	public ResponseEntity<?> UsuarioId(@Valid @PathVariable("id") String id){
		return this.personaimpl.PersonaporId(Long.parseLong(id));
	}
	
	@DeleteMapping("/eliminar/{id}")
	@ResponseBody
	public ResponseEntity<?> BorrarUsuarioId(@Valid @PathVariable("id") String id){
		return this.personaimpl.BorrarPersonaporId(Long.parseLong(id));
	}
	
	
}
