package pro.ciam.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CiamServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CiamServerApplication.class, args);
	}

}
